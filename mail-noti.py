import smtplib
import sys
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def send_email(subject, body):
    to_email = "email@got.co.th"
    smtp_server = "smtp-mail.outlook.com"  # Update this if you're using a different SMTP server
    smtp_port = 587  # Use 587 for TLS, 465 for SSL
    smtp_username = "email@got.co.th"
    smtp_password = "password"
    
    # Create the MIME object
    msg = MIMEMultipart()
    msg['From'] = smtp_username
    msg['To'] = to_email
    msg['Subject'] = subject

    # Attach the email body
    msg.attach(MIMEText(body, 'plain'))

    # Establish the SMTP connection
    with smtplib.SMTP(smtp_server, smtp_port) as server:
        # Log in to your Outlook account
        server.starttls()  # Use this line if you're using a secure connection (TLS)
        server.login(smtp_username, smtp_password)

        # Send the email
        server.sendmail(smtp_username, to_email, msg.as_string())

    print("Email notification sent successfully.")

# Call the function to send the email
# send_email(subject, body)

if __name__ == '__main__':
    globals()[sys.argv[1]](*sys.argv[2:])

# python mail-noti.py send_email "subject" "body"