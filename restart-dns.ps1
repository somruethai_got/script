# Define the username and password
$usernames = @("username01", "username02")
$passwords = @()

# Add passwords to the array
$passwords += ConvertTo-SecureString "password01" -AsPlainText -Force
$passwords += ConvertTo-SecureString "password02" -AsPlainText -Force

$hostname = "google.com"

# Define the remote server's hostname or IP address
$remoteServers = @("serverip01", "serverip02")

foreach ($i in 0..1) {
    $username = $usernames[$i]
    $password = $passwords[$i]
    $credential = New-Object System.Management.Automation.PSCredential($username, $password)

    $remoteServer = $remoteServers[$i]

    # Enable PowerShell Remoting on the remote server
    Invoke-Command -ComputerName $remoteServer -ScriptBlock {
        Enable-PSRemoting -Force
        Restart-Service -Name 'DNS'
        Get-Service | Where-Object { $_.DisplayName -like '*DNS*' }
    } -Credential $credential

    $result = nslookup $hostname

    # Check if nslookup was successful
    if ($result -match "Name:") {
        Write-Host "DNS resolution successful for $hostname."
    } else {
        Write-Host "DNS resolution failed for $hostname."

        # Set the timestamp
        $timestamp = Get-Date -Format "yyyy-MM-dd HH:mm:ss"

        # Set the email subject and body
        $emailSubject = "[PKS] DNS Resolution Failed on $remoteserver"
        $emailBody = "DNS resolution failed: $hostname on $remoteserver at $timestamp."

        # Send the email
        $pythonScriptPath = "C:\Users\admin\Desktop\script\mail-noti.py"
        $pythonExecutable = "python"
        $pythonCommand = "$pythonExecutable $pythonScriptPath send_email `"$emailSubject`" `"$emailBody`""

        # Invoke the Python script
        Invoke-Expression $pythonCommand
        break
    }
}
